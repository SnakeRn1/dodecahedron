#ifndef GRAPH_H
#define GRAPH_H

#include <QMainWindow>
#include <QVector>
#include <QHBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QVBoxLayout>
#include <QString>
#include <QTimer>
#include <iostream>
#include <cmath>

QT_BEGIN_NAMESPACE
namespace Ui { class Graph; }
QT_END_NAMESPACE

class Graph : public QMainWindow
{
    Q_OBJECT

public:
    Graph(QWidget *parent = nullptr);
    ~Graph();

private:

    QVector<QLabel*> icons;
    QVector<QLabel*> texts1;
    QVector<QLabel*> texts2;
    QVector<QLabel*> texts3;
    QVector<QLabel*> texts4;
    QVector<QHBoxLayout*> hlays;
    QVector<QHBoxLayout*> hlays_txt1;
    QVector<QHBoxLayout*> hlays_txt2;
    QVector<QVBoxLayout*> vlays;
    QVector<QProgressBar*> progs;
    QVector<QPushButton*> butts1;
    QVector<QPushButton*> butts2;
    QVector<QPushButton*> butts3;
    QVector<QPushButton*> butts4;
    QVector<double> how_many;
    QVector<double> price_costs;
    QVector<double> boosts;
    QVector<double> boost_costs;
    QVector<double> times_costs;
    QVector<double> perfs;
    QVector<int> counters;
    QVector<int> states;
    QVector<int> cycles;

    double last_res_cost;
    QTimer* timer_grafen;
    Ui::Graph *ui;
    int creation_status;
    double summ;
    int elms;

    void createElement(int i);
    QString get_summ_qstr(double);
    double get_boost_cost(int i);
    double get_perf(int i);
    double get_price_cost(int i);
    double get_times_cost(int i);
    void show_boost_cost(int i);
    void show_times_cost(int i);
    void show_price_cost(int i);
    QString get_name(int i);
    double get_res_cost();
private slots:
    void tim_func();
    void clicked();
    void on_researching_button_clicked();
};
#endif // GRAPH_H
