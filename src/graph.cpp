#include "graph.h"
#include "ui_graph.h"

using namespace std;

Graph::Graph(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Graph)
{
    ui->setupUi(this);

    elms = 13;

    texts1.resize(elms);
    texts2.resize(elms);
    texts3.resize(elms);
    texts4.resize(elms);
    hlays.resize(elms);
    hlays_txt1.resize(elms);
    hlays_txt2.resize(elms);
    vlays.resize(elms);
    progs.resize(elms);
    icons.resize(elms);
    butts1.resize(elms);
    butts2.resize(elms);
    butts3.resize(elms);
    butts4.resize(elms);
    how_many.resize(elms);
    price_costs.resize(elms);
    boosts.resize(elms);
    perfs.resize(elms);
    boost_costs.resize(elms);
    times_costs.resize(elms);
    counters.resize(elms);
    cycles.resize(elms);
    states.resize(elms);

    ui->researching_button->hide();
    creation_status = 0;
    createElement(creation_status);

    summ = 10.0;
    timer_grafen = new QTimer(this);
    connect(timer_grafen,SIGNAL(timeout()),this,SLOT(tim_func()));
    timer_grafen->start(100);
}

Graph::~Graph()
{
    delete ui;
}

void Graph::createElement(int i)
{
    how_many[i] = 0.0;
    boosts[i] = 1.0;
    perfs[i] = 0.0;
    counters[i] = 0;
    cycles[i] = 64;
    states[i] = -1;

    price_costs[i] = get_price_cost(i);
    boost_costs[i] = get_boost_cost(i);
    times_costs[i] = get_times_cost(i);

    hlays[i] = new QHBoxLayout();
    hlays[i]->setSpacing(5);
    hlays[i]->setObjectName("hlay_"+QString::number(i));

    icons[i] = new QLabel(ui->scrollAreaWidgetContents);
    icons[i]->setObjectName("icon_"+QString::number(i));
    icons[i]->setMinimumSize(QSize(80, 80));
    icons[i]->setMaximumSize(QSize(80, 80));
    icons[i]->setPixmap(QPixmap(":/"+QString::number(i+1)));
    icons[i]->setScaledContents(true);

    hlays[i]->addWidget(icons[i]);

    vlays[i] = new QVBoxLayout();
    vlays[i]->setSpacing(0);
    vlays[i]->setObjectName("vlay_"+QString::number(i));

    hlays_txt1[i] = new QHBoxLayout();
    hlays_txt1[i]->setSpacing(0);
    hlays_txt1[i]->setObjectName(QString::fromUtf8("horizontalLayout_2"));

    QFont font;
    font.setItalic(true);

    texts1[i] = new QLabel(ui->scrollAreaWidgetContents);
    texts1[i]->setObjectName("text_"+QString::number(i));
    texts1[i]->setMinimumSize(QSize(150, 30));
    texts1[i]->setMaximumSize(QSize(150, 30));
    texts1[i]->setFont(font);
    texts1[i]->setText(get_name(i));

    hlays_txt1[i]->addWidget(texts1[i]);

    texts3[i] = new QLabel(ui->scrollAreaWidgetContents);
    texts3[i]->setObjectName("text3_"+QString::number(i));
    texts3[i]->setMinimumSize(QSize(150, 30));
    texts3[i]->setMaximumSize(QSize(150, 30));
    texts3[i]->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

    hlays_txt1[i]->addWidget(texts3[i]);

    vlays[i]->addLayout(hlays_txt1[i]);

    progs[i] = new QProgressBar(ui->scrollAreaWidgetContents);
    progs[i]->setObjectName("prog_"+QString::number(i));
    progs[i]->setMinimumSize(QSize(300, 20));
    progs[i]->setMaximumSize(QSize(300, 20));
    progs[i]->setMaximum(cycles[i]);
    progs[i]->setValue(0);
    progs[i]->setAlignment(Qt::AlignCenter);

    vlays[i]->addWidget(progs[i]);

    hlays_txt2[i] = new QHBoxLayout();
    hlays_txt2[i]->setSpacing(0);
    hlays_txt2[i]->setObjectName(QString::fromUtf8("horizontalLayout_2"));

    QFont font2;
    font2.setPointSize(14);
    texts2[i] = new QLabel(ui->scrollAreaWidgetContents);
    texts2[i]->setObjectName("text2_"+QString::number(i));
    texts2[i]->setMinimumSize(QSize(150, 30));
    texts2[i]->setMaximumSize(QSize(150, 30));
    texts2[i]->setFont(font2);

    hlays_txt2[i]->addWidget(texts2[i]);

    texts4[i] = new QLabel(ui->scrollAreaWidgetContents);
    texts4[i]->setObjectName("text4_"+QString::number(i));
    texts4[i]->setMinimumSize(QSize(150, 30));
    texts4[i]->setMaximumSize(QSize(150, 30));
    texts4[i]->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

    hlays_txt2[i]->addWidget(texts4[i]);

    vlays[i]->addLayout(hlays_txt2[i]);

    hlays[i]->addLayout(vlays[i]);

    butts1[i] = new QPushButton(ui->scrollAreaWidgetContents);
    butts1[i]->setObjectName("but1_"+QString::number(i));
    butts1[i]->setMinimumSize(QSize(80, 80));
    butts1[i]->setMaximumSize(QSize(80, 80));
    show_price_cost(i);
    connect(butts1[i],SIGNAL(clicked()),this,SLOT(clicked()));

    hlays[i]->addWidget(butts1[i]);

    butts2[i] = new QPushButton(ui->scrollAreaWidgetContents);
    butts2[i]->setObjectName("but2_"+QString::number(i));
    butts2[i]->setMinimumSize(QSize(80, 80));
    butts2[i]->setMaximumSize(QSize(80, 80));
    show_boost_cost(i);
    connect(butts2[i],SIGNAL(clicked()),this,SLOT(clicked()));

    hlays[i]->addWidget(butts2[i]);

    butts3[i] = new QPushButton(ui->scrollAreaWidgetContents);
    butts3[i]->setObjectName("but3_"+QString::number(i));
    butts3[i]->setMinimumSize(QSize(80, 80));
    butts3[i]->setMaximumSize(QSize(80, 80));
    show_times_cost(i);
    connect(butts3[i],SIGNAL(clicked()),this,SLOT(clicked()));

    hlays[i]->addWidget(butts3[i]);

    butts4[i] = new QPushButton(ui->scrollAreaWidgetContents);
    butts4[i]->setObjectName("but4_"+QString::number(i));
    butts4[i]->setMinimumSize(QSize(80, 80));
    butts4[i]->setMaximumSize(QSize(80, 80));
    butts4[i]->setText("Заработать");
    show_times_cost(i);
    connect(butts4[i],SIGNAL(clicked()),this,SLOT(clicked()));

    hlays[i]->addWidget(butts4[i]);

    ui->verticalLayout->addLayout(hlays[i]);
}

void Graph::show_price_cost (int i)
{
    butts1[i]->setText("Купить\r\nза\r\n"+get_summ_qstr(price_costs[i]));
}

void Graph::show_boost_cost (int i)
{
    butts2[i]->setText("Доход\r\nx"+QString::number(boosts[i]*2.0)+"\r\n"+get_summ_qstr(get_boost_cost(i)));
}

void Graph::show_times_cost (int i)
{
    if(states[i] == -1) butts3[i]->setText("Купить\r\nавтомат\r\n"+get_summ_qstr(get_times_cost(i)));
    if(states[i] == 0) butts3[i]->setText("Ускорить\r\n"+get_summ_qstr(get_times_cost(i)));
}

QString Graph::get_name(int i)
{
    switch (i+1)
    {
        case 1:  return "1:  Cuboctahedron";
        case 2:  return "2:  Great_rhombicosidodecahedron";
        case 3:  return "3:  Great_rhombicuboctahedron";
        case 4:  return "4:  Icosidodecahedron";
        case 5:  return "5:  Small_rhombicosidodecahedron";
        case 6:  return "6:  Small_rhombicuboctahedron";
        case 7:  return "7:  Snub_dodecahedron_ccw";
        case 8:  return "8:  Snub_hexahedron";
        case 9:  return "9:  Truncated_dodecahedron";
        case 10: return "10: Truncated_hexahedron";
        case 11: return "11: Truncated_icosahedron";
        case 12: return "12: Truncated_octahedron";
        case 13: return "13: Truncated_tetrahedron";
    default: return " ";
    }
}

void Graph::clicked()
{
    QString name = sender()->objectName();
    if (name.contains("but1"))
    {
        int i = name.remove(0,5).toInt();
        if(summ>=price_costs[i])
        {
            summ -= price_costs[i];
            how_many[i]++;
            if(how_many[i] == 8)
            {
                ui->researching_button->show();
                if(creation_status<12)
                {
                    last_res_cost = get_res_cost();
                    ui->researching_button->setText("Разработать элемент. Стоимость: "+get_summ_qstr(last_res_cost));
                }
                else ui->researching_button->setText("Вы полностью прошли игру!");
            }
            texts2[i]->setText(QString::number(how_many[i]));
            price_costs[i] = get_price_cost(i);
            show_price_cost(i);
            perfs[i] = get_perf(i);
            texts3[i]->setText("Единичный: "+get_summ_qstr(perfs[i]/how_many[i]));
            texts4[i]->setText("Суммарный: "+get_summ_qstr(perfs[i]));
        }
    }
    else if (name.contains("but2"))
    {
        int i = name.remove(0,5).toInt();
        if(summ>=boost_costs[i])
        {
            summ -= boost_costs[i];
            boosts[i] *=2;
            boost_costs[i] = get_boost_cost(i);

            perfs[i] = get_perf(i);
            show_boost_cost(i);
            texts3[i]->setText("Единичный: "+get_summ_qstr(perfs[i]/how_many[i]));
            texts4[i]->setText("Суммарный: "+get_summ_qstr(perfs[i]));
        }
    }
    else if (name.contains("but3"))
    {
        int i = name.remove(0,5).toInt();
        if(summ>=times_costs[i])
        {
            if(states[i] == -1)
            {
                summ -= times_costs[i];
                states[i] = 0;
                counters[i] = 1;
                progs[i]->setValue(counters[i]);
                times_costs[i] = get_times_cost(i);
                show_times_cost(i);
                butts4[i]->setText("AUTO");
            }
            else if(states[i] == 0)
            {
                if(cycles[i] > 2)
                {
                    summ -= times_costs[i];
                    cycles[i] /= 2;
                    counters[i] = 1;
                    progs[i]->setMaximum(cycles[i]);
                    progs[i]->setValue(counters[i]);
                    times_costs[i] = get_times_cost(i);
                    if(cycles[i] == 2) butts3[i]->setText("Каждый цикл\r\n"+get_summ_qstr(times_costs[i]));
                    else show_times_cost(i);
                    ui->statusbar->showMessage("Всего денег: "+get_summ_qstr(summ));
                }
                else
                {
                    summ -= times_costs[i];
                    states[i] = 1;
                    cycles[i] = 1;
                    counters[i] = 1;
                    progs[i]->setMaximum(1);
                    progs[i]->setValue(1);
                    progs[i]->setFormat("auto every cycle");
                    butts3[i]->setText("MAX");
                    ui->statusbar->showMessage("Всего денег: "+get_summ_qstr(summ));
                }
            }
        }
    }
    else if (name.contains("but4"))
    {
        int i = name.remove(0,5).toInt();
        if(counters[i] == 0) counters[i] = 1;
    }
}

QString Graph::get_summ_qstr(double summ)
{
    if(summ>=1000.0)
    {
        if(summ>=1000000.0)
        {
            if(summ>=1000000000.0)
            {
                if(summ>=1000000000000.0)
                {
                    return QString::number(summ/1000000000000.0,'f',0)+" Q";
                }
                else
                {
                    return QString::number(summ/1000000000.0,'f',3)+" T";
                }
            }
            else
            {
                return QString::number(summ/1000000.0,'f',2)+" M";
            }
        }
        else
        {
            return QString::number(summ/1000.0,'f',1)+" K";
        }
    }
    else
    {
        return QString::number(summ,'f',1);
    }
}

double Graph::get_price_cost(int i)
{
    return pow(1.4,(how_many[i]+1)*(i+1))/(i+1);
}

double Graph::get_boost_cost(int i)
{
    return pow(1.4,32+boosts[i]*(i+1)*0.5)/(i+1);
}

double Graph::get_times_cost(int i)
{
    return pow(1.4,8*(i+1))*(64/cycles[i])/(i+1);
}

double Graph::get_perf(int i)
{
    return pow(2.6,(i+i))*how_many[i]*boosts[i];
}

double Graph::get_res_cost()
{
    return pow(1.4,8*(creation_status+1))/(creation_status+2);
}

void Graph::tim_func()
{
    double full = 0.0;
    for (int i(0);i<=creation_status;++i)
    {
        if(states[i]<=0)
        {
            if(counters[i]>0)
            {
                if(counters[i]>=cycles[i])
                {
                    summ += perfs[i];
                    if(states[i] == -1) counters[i] = 0;
                    if(states[i] == 0) counters[i] = 1;
                }
                else
                {
                    counters[i]++;
                }
                progs[i]->setValue(counters[i]);
            }
        }
        else
        {
            summ += perfs[i];
        }

        full += perfs[i]/cycles[i];

        if (summ>=price_costs[i]) butts1[i]->setDisabled(false);
        else butts1[i]->setDisabled(true);

        if(summ>=boost_costs[i]) butts2[i]->setDisabled(false);
        else butts2[i]->setDisabled(true);

        if((states[i]<=0)&&(summ>=times_costs[i])&&(how_many[i]>0)) butts3[i]->setDisabled(false);
        else butts3[i]->setDisabled(true);

        if((how_many[i]>0)&&(counters[i]==0)) butts4[i]->setDisabled(false);
        else butts4[i]->setDisabled(true);

        if(summ>=last_res_cost) ui->researching_button->setDisabled(false);
        else ui->researching_button->setDisabled(true);
    }
    ui->statusbar->showMessage("Всего денег: " + get_summ_qstr(summ) + "   Полный доход на цикл: " + get_summ_qstr(full) + "   Примерный доход в секунду: " + get_summ_qstr(full*10.0));
}

void Graph::on_researching_button_clicked()
{
    if((summ>=last_res_cost)&&(creation_status<12))
    {
        summ-=last_res_cost;
        creation_status++;
        createElement(creation_status);
        if(creation_status == 12) ui->researching_button->setText("Вы открыли все камни, поздравляем!!!");
        else ui->researching_button->hide();
    }
}
